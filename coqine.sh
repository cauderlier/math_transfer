#!/usr/bin/env bash
# Copyright Ali Assaf <ali.assaf@inria.fr>
#           and Raphaël Cauderlier <raphael.cauderlier@inria.fr>, 2015

#    This software is governed by the CeCILL-B license under French law and
# abiding by the rules of distribution of free software.

# See "http://www.cecill.info".

( echo "Require Coqine."
  for arg
  do
    echo "Require $arg."
  done
  echo "Dedukti Export All."
)
