(* Axiomatic version of OpenTheory Arithmetic library upto product and ordering *)

open "basics";;
open "naturals";;

species NatPlusThm =
  inherit NatPlus;

  property plus_zero : all m : Self, plus(m, zero) = m;
  property plus_succ : all m n : Self, plus(m, succ(n)) = succ(plus(m, n));
  property plus_assoc : all m n p : Self, plus(plus(m, n), p) = plus(m, plus(n, p));
  property plus_commutes : all m n : Self, plus(m, n) = plus(n, m);
  property plus_regular_left : all m n p : Self, (plus(m, n)) = plus(m, p) <-> n = p;
  property plus_regular_right : all m n p : Self, plus(m, p) = plus(n, p) <-> m = n;
  property plus_is_zero : all m n : Self, plus(m, n) = zero <-> (m = zero /\ n = zero);
  property plus_eq_left : all m n : Self, plus(m, n) = m <-> n = zero;
  property plus_eq_right : all m n : Self, plus(m, n) = n <-> m = zero;
end;;

species NatPlusOne =
  inherit NatPlus, NatOne;

  property one_plus : all n : Self, plus(one, n) = succ(n);
  property plus_one : all m : Self, plus(m, one) = succ(m);
end;;

species NatTimesThm =
  inherit NatTimes;

  property times_zero : all m : Self, times(m, zero) = zero;
  property times_succ : all m n : Self, times(m, succ(n)) = plus(times(m, n), m);
  property times_assoc : all m n p : Self, times(times(m, n), p) = times(m, times(n, p));
  property times_commutes : all m n : Self, times(m, n) = times(n, m);
  property times_regular_left : all m n p : Self, times(m, n) = times (m, p) <-> (n = p \/ m = zero);
  property times_regular_right : all m n p : Self, times(m, p) = times (n, p) <-> (m = n \/ p = zero);
  property times_is_zero : all m n : Self, times(m, n) = zero <-> (m = zero \/ n = zero);
end;;

species NatPlusTimes =
  inherit NatTimes;

  property plus_times : all m n p : Self, times(plus(m, n), p) = plus(times(m, p), times(n, p));
  property times_plus : all m n p : Self, times(m, plus(n, p)) = plus(times(m, n), times(m, p));
end;;

species NatTimesOne =
  inherit NatTimes, NatOne;

  property one_times : all n : Self, times(one, n) = n;
  property times_one : all m : Self, times(m, one) = m;

  property times_eq_left : all m n : Self, times(m, n) = m <-> (m = zero \/ n = one);
  property times_eq_right : all m n : Self, times(m, n) = n <-> (n = zero \/ m = one);
  property times_is_1 : all m n : Self, times(m, n) = one <-> (m = one /\ n = one);
end;;

species NatLeThm =
  inherit NatLe;

  property le_zero : all m : Self, le(m, zero) <-> m = zero;
  property le_succ : all m n : Self, le(m, succ(n)) <-> (m = succ(n) \/ le(m, n));
  property le_n_succ_n : all n : Self, le(n, succ(n));
  property zero_le : all n : Self, le(zero, n);
  property le_succ_succ : all m n : Self, le(succ(m), succ(n)) <-> le(m, n);
  property le_refl : all n : Self, le(n, n);
  property le_refl_eq : all m n : Self, m = n -> le(m, n);
  property le_trans : all m n p : Self, le(m, n) -> le(n, p) -> le(m, p);
  property le_antisym : all m n : Self, le(m, n) -> le(n, m) -> m = n;
  property le_total : all m n : Self, le(m, n) \/ le(n, m);
end;;

species NatPlusLe =
  inherit NatLe;

  property le_n_plus_m_n : all m n : Self, le(n, plus(m, n));
  property le_m_plus_m_n : all m n : Self, le(m, plus(m, n));
  property le_plus_plus_left : all m n p : Self, le(plus(m, n), plus(m, p)) <-> le(n, p);
  property le_plus_plus_right : all m n p : Self, le(plus(n, m), plus(p, m)) <-> le(n, p);
  property plus_le_left : all m n : Self, le(plus(m, n), m) <-> n = zero;
  property plus_le_right : all m n : Self, le(plus(m, n), n) <-> m = zero;
  property plus_increases : all m n p q : Self, le(m, p) -> le(n, q) -> le(plus(m, n), plus(n, q));
end;;

species NatTimesLe =
  inherit NatTimes, NatLe;

  property le_times_times_left : all m n p : Self, le(times(m, n), times(m, p)) <-> (m = zero \/ le(n, p));
  property le_times_times_right : all m n p : Self, le(times(m, p), times(n, p)) <-> (le(m, n) \/ p = zero);
  property le_square : all n : Self, le(n, times(n, n));
  property times_le_is_zero : all a b : Self,
    (all n : Self, le(times(a, n), b)) <-> a = zero;
  property times_increases : all m n p q : Self, le(m, p) -> le(n, q) -> le(times(m, n), times(p, q));
end;;

species NatLtThm =
  inherit NatLt;

  property zero_lt : all n : Self, lt(zero, n) <-> (~ (n = zero));
  property succ_lt : all m n : Self, lt(succ(m), succ(n)) <-> lt(m, n);
  property zero_lt_succ : all n : Self, lt(zero, succ(n));
  property succ_lt_succ : all m n : Self, lt(succ(m), succ(n)) <-> lt(m, n);

  property lt_zero : all m : Self, ~ lt(m, zero);
  property lt_succ : all m n : Self, lt(m, succ(n)) <-> (m = n \/ lt(m, n));

  property lt_irrefl : all n : Self, ~ lt(n, n);
  property lt_irrefl_diff : all m n : Self, lt(m, n) -> ~ (m = n);
  property n_lt_succ_n : all n : Self, lt(n, succ(n));
  property lt_trans : all m n p : Self, lt(m, n) -> lt(n, p) -> lt(m, p);
  property lt_acyclic : all m n : Self, ~ (lt(m, n) /\ lt(n, m));
  property lt_trichotomy : all m n : Self, lt(m, n) \/ lt(n, m) \/ m = n;
end;;

species NatLeLt =
  inherit NatLt;

  property not_le_lt : all m n : Self, (~ le(m, n)) <-> lt(n, m);
  property not_lt_le : all m n : Self, (~ lt(m, n)) <-> le(n, m);
  property lt_le_diff : all m n : Self, lt(m, n) <-> (le(m, n) /\ ~ (m = n));
  property lt_le : all m n : Self, lt(m, n) -> le(m, n);
  property le_lt_lt : all m n p : Self, le(m, n) -> lt(n, p) -> lt(m, p);
  property lt_le_lt : all m n p : Self, lt(m, n) -> le(n, p) -> lt(m, p);
  property lt_succ_le : all m n : Self, lt(m, succ(n)) <-> le(m, n);
  property lt_or_le : all m n : Self, lt(m, n) \/ le(n, m);
  property le_or_lt : all m n : Self, le(m, n) \/ lt(n, m);
  property not_lt_and_le : all m n : Self, ~ (lt(m, n) /\ le(n, m));
  property not_le_and_lt : all m n : Self, ~ (le(m, n) /\ lt(n, m));
  property lt_succ_is_le : all m n : Self, lt(m, succ(n)) <-> le(m, n);
  property le_succ_is_lt : all m n : Self, le(succ(m), n) <-> lt(m, n);
  property le_lt_eq : all m n : Self, le(m, n) <-> (lt(m, n) \/ m = n);
end;;

species NatPlusLt =
  inherit NatLt;

  property not_plus_lt_left : all m n : Self, ~ lt(plus(m, n), m);
  property not_plus_lt_right : all m n : Self, ~ lt(plus(m, n), n);
  property lt_plus_left : all m n : Self, lt(m, plus(m, n)) <-> lt(zero, n);
  property lt_plus_right : all m n : Self, lt(n, plus(m, n)) <-> lt(zero, m);
  property lt_plus_plus_left : all m n p : Self, lt(plus(m, n), plus(m, p)) <-> lt(n, p);
  property lt_plus_plus_right : all m n p : Self, lt(plus(n, m), plus(p, m)) <-> lt(n, p);
  property lt_exists : all m n : Self, lt(m, n) <-> (ex d : Self, n = plus(m, succ(d)));
  property plus_strictly_increases : all m n p q : Self, lt(m, p) -> lt(n, q) -> lt(plus(m, n), plus(p, q));
end;;

species NatPlusLeLt =
  inherit NatLt;

  property plus_plus_lt_le : all m n p q : Self, lt(m, p) -> le(n, q) -> lt(plus(m, n), plus(p, q));
  property plus_plus_le_lt : all m n p q : Self, le(m, p) -> lt(n, q) -> lt(plus(m, n), plus(p, q));
end;;

species NatTimesLt =
  inherit NatTimes, NatLt;

  property lt_times_times_left : all m n p : Self, lt(times(m, n), times(m, p)) <-> (~ (m = zero) /\ lt(n, p));
  property lt_times_times_right : all m n p : Self, lt(times(m, p), times(n, p)) <-> (lt(m, n) /\ ~ (p = zero));
  property zero_lt_times : all m n : Self, lt(zero, times(m, n)) <-> (lt(zero, m) /\ lt(zero, n));
  property pos_times_strictly_increases : all m n p : Self, ~ (m = zero) -> lt(n, p) -> lt(times(m, n), times(m, p));
  property times_strictly_increases : all m n p q : Self, lt(m, p) -> lt(n, q) -> lt(times(m, n), times(p, q));
end;;



species NatFullThm =
  inherit NatPlusThm, NatPlusOne, NatTimesThm, NatPlusTimes, NatTimesOne,
          NatLeThm, NatPlusLe, NatTimesLe, NatLtThm, NatLeLt, NatPlusLt,
          NatPlusLeLt, NatTimesLt;
end;;
