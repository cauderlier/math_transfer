open "basics";;
open "naturals";;

species NatMorph (A is NatDecl) =
  inherit NatDecl;

  signature morph : A -> Self;
  property morph_zero : morph(A!zero) = zero;
  property morph_succ : all a : A, morph(A!succ(a)) = succ(morph(a));

  logical let has_antecedent (b : Self) = ex a : A, b = morph(a);

  theorem has_antecedent_zero : has_antecedent(zero)
  proof = by definition of has_antecedent property morph_zero;

  theorem has_antecedent_succ : all b : Self,
      has_antecedent(b) -> has_antecedent(succ(b))
  proof = by definition of has_antecedent property morph_succ;

  property have_antecedents : all b : Self, has_antecedent(b);
  (* proof = *)
  (* dedukti proof *)
  (*   property ind, has_antecedent_zero, has_antecedent_succ *)
  (*   {* abst_ind abst_has_antecedent abst_has_antecedent_zero abst_has_antecedent_succ. *}; *)

  theorem morph_surjective : all b : Self, ex a : A, b = morph(a)
  proof = by definition of has_antecedent property have_antecedents;

  property morph_injective : all a1 a2 : A,
     morph(a1) = morph(a2) -> a1 = a2;
  (* proof = *)
  (* <1>1 prove (all a2 : A, morph(A!zero) = morph(a2) -> A!zero = a2) -> *)
  (*            (all a1 : A, *)
  (*              (all a2 : A, morph(a1) = morph(a2) -> a1 = a2) -> *)
  (*              (all a2 : A, morph(A!succ(a1)) = morph(a2) -> A!succ(a1) = a2)) -> *)
  (*            (all a1 a2 : A, morph(a1) = morph(a2) -> a1 = a2) *)
  (*      dedukti proof property A!ind *)
  (*        {* _p_A_ind (a1 : cc.eT _p_A_T => dk_logic.forall _p_A_T ( *)
  (*                     a2 : cc.eT _p_A_T => *)
  (*                     dk_logic.imp (dk_logic.eq abst_T (abst_morph a1) (abst_morph a2)) *)
  (*                             (dk_logic.eq _p_A_T a1 a2))) *} *)
  (* <1>2 assume a2 : A, *)
  (*      hypothesis H : morph(A!zero) = morph(a2), *)
  (*      prove A!zero = a2 *)
  (*      <2>1 prove a2 = A!zero \/ ex a : A, a2 = A!succ(a) *)
  (*           by property A!case *)
  (*      <2>2 assume a : A, *)
  (*           hypothesis H1 : a2 = A!succ(a), *)
  (*           prove false *)
  (*           <3>1 prove morph(a2) = zero *)
  (*                by hypothesis H property morph_zero *)
  (*           <3>2 prove morph(a2) = succ(morph(a)) *)
  (*                by hypothesis H1 property morph_succ *)
  (*           <3>3 prove ~ (zero = succ(morph(a))) *)
  (*                by property zero_succ *)
  (*           <3>f conclude *)
  (*      <2>f conclude *)
  (* <1>3 assume a1 a2 : A, *)
  (*      hypothesis IH : all a2 : A, morph(a1) = morph(a2) -> a1 = a2, *)
  (*      hypothesis H1 : morph(A!succ(a1)) = morph(a2), *)
  (*      prove A!succ(a1) = a2 *)
  (*      <2>1 prove a2 = A!zero \/ ex a : A, a2 = A!succ(a) *)
  (*           by property A!case *)
  (*      <2>2 hypothesis H2 : a2 = A!zero, *)
  (*           prove false *)
  (*           <3>1 prove morph(A!succ(a1)) = morph(A!zero) *)
  (*                by hypothesis H1, H2 *)
  (*           <3>2 prove succ(morph(a1)) = zero *)
  (*                by step <3>1 property morph_zero, morph_succ *)
  (*           <3>3 prove ~ succ(morph(a1)) = zero *)
  (*                by property zero_succ *)
  (*           <3>f conclude *)
  (*      <2>3 assume a : A, *)
  (*           hypothesis H2 : a2 = A!succ(a), *)
  (*           prove A!succ(a1) = a2 *)
  (*           <3>1 prove morph(A!succ(a1)) = morph(A!succ(a)) *)
  (*                by hypothesis H1, H2 *)
  (*           <3>2 prove succ(morph(a1)) = succ(morph(a)) *)
  (*                by step <3>1 property morph_succ *)
  (*           <3>3 prove succ(morph(a1)) = succ(morph(a)) -> *)
  (*                      morph(a1) = morph(a) *)
  (*                by property succ_inj *)
  (*           <3>4 prove morph(a1) = morph(a) *)
  (*                by step <3>2, <3>3 *)
  (*           <3>5 prove a1 = a *)
  (*                by step <3>4 hypothesis IH *)
  (*           <3>6 prove A!succ(a1) = a2 *)
  (*                by step <3>5 hypothesis H2 *)
  (*           <3>f conclude *)
  (*      <2>f conclude *)
  (* <1>f conclude; *)
end;;

species NatOneMorph (A is NatOne) =
  inherit NatOne, NatMorph(A);

  theorem morph_one : morph(A!one) = one
  proof = by property one_spec, morph_zero, morph_succ, A!one_spec;
end;;


species NatPlusMorph (A is NatPlus) =
  inherit NatPlus, NatMorph(A);

  logical let morph_plus_predicate (a1 : A) =
     all a2 : A, morph(A!plus(a1, a2)) = plus(morph(a1), morph(a2));

  theorem morph_zero_plus : morph_plus_predicate(A!zero)
  proof = by definition of morph_plus_predicate
             property zero_plus, morph_zero, A!zero_plus;

  theorem morph_succ_plus :
     all a : A, morph_plus_predicate(a) -> morph_plus_predicate(A!succ(a))
  proof =
  <1>1 assume a1 a2 : A,
       hypothesis H : morph(A!plus(a1, a2)) = plus(morph(a1), morph(a2)),
       prove morph(A!plus(A!succ(a1), a2)) = plus(morph(A!succ(a1)), morph(a2))
       <2>1 prove A!plus(A!succ(a1), a2) = A!succ(A!plus(a1, a2))
            by property A!succ_plus
       <2>2 prove morph(A!plus(A!succ(a1), a2)) = morph(A!succ(A!plus(a1, a2)))
            by step <2>1
       <2>3 prove morph(A!succ(A!plus(a1, a2))) = succ(morph(A!plus(a1, a2)))
            by property morph_succ
       <2>4 prove succ(morph(A!plus(a1, a2))) = succ(plus(morph(a1), morph(a2)))
            by hypothesis H
       <2>5 prove succ(plus(morph(a1), morph(a2))) = plus(succ(morph(a1)), morph(a2))
            by property succ_plus
       <2>6 prove succ(morph(a1)) = morph(A!succ(a1))
            by property morph_succ
       <2>7 prove plus(succ(morph(a1)), morph(a2)) = plus(morph(A!succ(a1)), morph(a2))
            by step <2>6
       <2>f conclude
  <1>f qed by step <1>1 definition of morph_plus_predicate;

  property morph_plus_with_predicate : all a : A, morph_plus_predicate(a);
  (* proof = *)
  (* dedukti proof *)
  (*   property A!ind, morph_zero_plus, morph_succ_plus *)
  (*   {* _p_A_ind abst_morph_plus_predicate abst_morph_zero_plus abst_morph_succ_plus. *}; *)

  theorem morph_plus : all a1 a2 : A,
          morph(A!plus(a1, a2)) = plus(morph(a1), morph(a2))
  proof = by definition of morph_plus_predicate property morph_plus_with_predicate;

end;;

species NatTimesMorph (A is NatTimes) =
  inherit NatTimes, NatPlusMorph(A);

  logical let morph_times_predicate (a1 : A) =
     all a2 : A, morph(A!times(a1, a2)) = times(morph(a1), morph(a2));

  theorem morph_zero_times : morph_times_predicate(A!zero)
  proof = by definition of morph_times_predicate
             property A!zero_times, morph_zero, zero_times;

  theorem morph_succ_times :
     all a : A, morph_times_predicate(a) -> morph_times_predicate(A!succ(a))
  proof =
  <1>1 assume a1 a2 : A,
       hypothesis H : morph(A!times(a1, a2)) = times(morph(a1), morph(a2)),
       prove morph(A!times(A!succ(a1), a2)) = times(morph(A!succ(a1)), morph(a2))
       <2>1 prove A!times(A!succ(a1), a2) = A!plus(a2, A!times(a1, a2))
            by property A!succ_times
       <2>2 prove morph(A!times(A!succ(a1), a2)) = morph(A!plus(a2, A!times(a1, a2)))
            by step <2>1
       <2>3 prove morph(A!plus(a2, A!times(a1, a2))) = plus(morph(a2), morph(A!times(a1, a2)))
            by property morph_plus
       <2>4 prove plus(morph(a2), morph(A!times(a1, a2))) = plus(morph(a2), times(morph(a1), morph(a2)))
            by hypothesis H
       <2>5 prove plus(morph(a2), times(morph(a1), morph(a2))) = times(succ(morph(a1)), morph(a2))
            by property succ_times
       <2>6 prove succ(morph(a1)) = morph(A!succ(a1))
            by property morph_succ
       <2>7 prove times(succ(morph(a1)), morph(a2)) = times(morph(A!succ(a1)), morph(a2))
            by step <2>6
       <2>f conclude
  <1>f qed by step <1>1 definition of morph_times_predicate;

  property morph_times_with_predicate : all a : A, morph_times_predicate(a);
  (* proof = *)
  (* dedukti proof *)
  (*   property A!ind, morph_zero_times, morph_succ_times *)
  (*   {* _p_A_ind abst_morph_times_predicate abst_morph_zero_times abst_morph_succ_times. *}; *)

  theorem morph_times : all a1 a2 : A,
          morph(A!times(a1, a2)) = times(morph(a1), morph(a2))
  proof = by definition of morph_times_predicate property morph_times_with_predicate;
end;;

species NatLeMorph(A is NatLe) =
  inherit NatLe, NatPlusMorph(A);

  theorem morph_le :
     all a1 a2 : A,
        A!le(a1, a2) -> le(morph(a1), morph(a2))
  proof =
  <1>1 assume a1 a2 : A,
       hypothesis H : A!le(a1, a2),
       prove le(morph(a1), morph(a2))
       <2>1 prove (ex a3 : A, A!plus(a1, a3) = a2)
            by property A!le_plus hypothesis H
       <2>2 prove (ex a3 : A, morph(A!plus(a1, a3)) = morph(a2))
            by step <2>1
       <2>3 prove (ex a3 : A, plus(morph(a1), morph(a3)) = morph(a2))
            by step <2>2 property morph_plus
       <2>4 prove (ex b : Self, plus(morph(a1), b) = morph(a2))
            by step <2>3
       <2>5 prove le(morph(a1), morph(a2))
            by step <2>4 property le_plus
       <2>f conclude
  <1>2 conclude;

  theorem morph_le_rev :
     all a1 a2 : A,
        le(morph(a1), morph(a2)) -> A!le(a1, a2)
  proof =
  <1>1 assume a1 a2 : A,
       hypothesis H : le(morph(a1), morph(a2)),
       prove A!le(a1, a2)
       <2>1 prove (ex b : Self, plus(morph(a1), b) = morph(a2))
            by hypothesis H property le_plus
       <2>2 prove (ex a3 : A, plus(morph(a1), morph(a3)) = morph(a2))
            by step <2>1 property morph_surjective
       <2>3 prove (ex a3 : A, morph(A!plus(a1, a3)) = morph(a2))
            by step <2>2 property morph_plus
       <2>4 prove (ex a3 : A, A!plus(a1, a3) = a2)
            <3>1 assume a3 : A,
                 notation a = A!plus(a1, a3),
                 hypothesis H1 : morph(A!plus(a1, a3)) = morph(a2),
                 prove A!plus(a1, a3) = a2
                 <4>1 prove morph(a) = morph(a2)
                      by hypothesis H1 definition of a
                 <4>2 prove morph(a) = morph(a2) -> a = a2
                      dedukti proof
                         property morph_injective
                         {* abst_morph_injective a a2 *}
                 <4>3 qed by step <4>1, <4>2 definition of a
            <3>2 qed by step <3>1, <2>3
       <2>f qed by step <2>4 property A!le_plus
  <1>2 conclude;
end;;

species NatLtMorph(A is NatLt) =
  inherit NatLt, NatLeMorph(A);

  theorem morph_lt :
     all a1 a2 : A,
        A!lt(a1, a2) -> lt(morph(a1), morph(a2))
  proof =
  <1>1 assume a1 a2 : A,
       hypothesis H : A!lt(a1, a2),
       prove lt(morph(a1), morph(a2))
       <2>1 prove A!le(A!succ(a1), a2)
            by property A!lt_spec hypothesis H
       <2>2 prove le(morph(A!succ(a1)), morph(a2))
            by property morph_le step <2>1
       <2>3 prove le(succ(morph(a1)), morph(a2))
            by property morph_succ step <2>2
       <2>4 prove lt(morph(a1), morph(a2))
            by property lt_spec step <2>3
       <2>f conclude
  <1>2 conclude;

  theorem morph_lt_rev :
     all a1 a2 : A,
        lt(morph(a1), morph(a2)) ->
        A!lt(a1, a2)
  proof =
  <1>1 assume a1 a2 : A,
       hypothesis H : lt(morph(a1), morph(a2)),
       prove A!lt(a1, a2)
       <2>1 prove le(succ(morph(a1)), morph(a2))
            by property lt_spec hypothesis H
       <2>2 prove le(morph(A!succ(a1)), morph(a2))
            by property morph_succ step <2>1
       <2>3 prove A!le(A!succ(a1), a2)
            by property morph_le_rev step <2>2
       <2>4 prove A!lt(a1, a2)
            by property A!lt_spec step <2>3
       <2>f conclude
  <1>2 conclude;
end;;

(* species NatDividesMorph(B is NatDivides) = *)
(*   inherit NatDivides, NatTimesMorph(B), NatLtMorph(B); *)

(*   theorem morph_divides : *)
(*      all a1 a2 : Self, *)
(*         divides(a1, a2) -> B!divides(morph(a1), morph(a2)) *)
(*   proof = *)
(*   <1>1 assume a1 a2 : Self, *)
(*        hypothesis H : divides(a1, a2), *)
(*        prove B!divides(morph(a1), morph(a2)) *)
(*        <2>1 prove (ex a3 : Self, times(a1, a3) = a2) *)
(*             by property divides_times hypothesis H *)
(*        <2>2 prove (ex a3 : Self, morph(times(a1, a3)) = morph(a2)) *)
(*             by step <2>1 *)
(*        <2>3 prove (ex a3 : Self, B!times(morph(a1), morph(a3)) = morph(a2)) *)
(*             by step <2>2 property morph_times *)
(*        <2>4 prove (ex b : B, B!times(morph(a1), b) = morph(a2)) *)
(*             by step <2>3 *)
(*        <2>5 prove B!divides(morph(a1), morph(a2)) *)
(*             by step <2>4 property B!divides_times *)
(*        <2>f conclude *)
(*   <1>2 conclude; *)

(*   theorem morph_divides_rev : *)
(*      all a1 a2 : Self, *)
(*         B!divides(morph(a1), morph(a2)) -> divides(a1, a2) *)
(*   proof = *)
(*   <1>1 assume a1 a2 : Self, *)
(*        hypothesis H : B!divides(morph(a1), morph(a2)), *)
(*        prove divides(a1, a2) *)
(*        <2>1 prove (ex b : B, B!times(morph(a1), b) = morph(a2)) *)
(*             by hypothesis H property B!divides_times *)
(*        <2>2 prove (ex a3 : Self, B!times(morph(a1), morph(a3)) = morph(a2)) *)
(*             by step <2>1 property morph_surjective *)
(*        <2>3 prove (ex a3 : Self, morph(times(a1, a3)) = morph(a2)) *)
(*             by step <2>2 property morph_times *)
(*        <2>4 prove (ex a3 : Self, times(a1, a3) = a2) *)
(*             <3>1 assume a3 : Self, *)
(*                  notation a = times(a1, a3), *)
(*                  hypothesis H1 : morph(times(a1, a3)) = morph(a2), *)
(*                  prove times(a1, a3) = a2 *)
(*                  <4>1 prove morph(a) = morph(a2) *)
(*                       by hypothesis H1 definition of a *)
(*                  <4>2 prove morph(a) = morph(a2) -> a = a2 *)
(*                       dedukti proof *)
(*                          property morph_injective *)
(*                          {* abst_morph_injective a a2 *} *)
(*                  <4>3 qed by step <4>1, <4>2 definition of a *)
(*             <3>2 qed by step <3>1, <2>3 *)
(*        <2>f qed by step <2>4 property divides_times *)
(*   <1>2 conclude; *)

(*   (\* proof of divides_le = *\) *)
(*   (\* <1>1 assume m n : Self, *\) *)
(*   (\*      hypothesis H1 : divides(m, n), *\) *)
(*   (\*      hypothesis H2 : le(succ(succ(zero)), n), *\) *)
(*   (\*      prove le(m, n) *\) *)
(*   (\*      <2>1 prove B!divides(morph(m), morph(n)) *\) *)
(*   (\*           by hypothesis H1 property morph_divides *\) *)
(*   (\*      <2>2 prove B!le(B!succ(B!succ(B!zero)), morph(n)) *\) *)
(*   (\*           by hypothesis H2 property morph_zero, morph_succ, morph_le *\) *)
(*   (\*      <2>3 prove B!le(morph(m), morph(n)) *\) *)
(*   (\*           by step <2>1, <2>2 property B!divides_le *\) *)
(*   (\*      <2>4 prove B!le(morph(m), morph(n)) -> le(m, n) *\) *)
(*   (\*           by property morph_le_rev *\) *)
(*   (\*      <2>f conclude *\) *)
(*   (\* <1>f conclude; *\) *)
(* end;; *)



(* species NatSDMorph (B is NatStrictlyDivides) = *)
(*   inherit NatStrictlyDivides, NatDividesMorph(B), NatLtMorph(B); *)

(*   theorem morph_sd : *)
(*      all a1 a2 : Self, *)
(*        sd(a1, a2) -> B!sd(morph(a1), morph(a2)) *)
(*   proof = *)
(*   <1>1 assume a1 a2 : Self, *)
(*        hypothesis H : sd(a1, a2), *)
(*        prove B!sd(morph(a1), morph(a2)) *)
(*        <2>1 prove divides(a1, a2) *)
(*             by property sd_divides hypothesis H *)
(*        <2>2 prove B!divides(morph(a1), morph(a2)) *)
(*             by property morph_divides step <2>1 *)
(*        <2>3 prove lt(a1, a2) *)
(*             by property sd_lt hypothesis H *)
(*        <2>4 prove B!lt(morph(a1), morph(a2)) *)
(*             by property morph_lt step <2>3 *)
(*        <2>5 prove lt(succ(zero), a1) *)
(*             by property sd_1 hypothesis H *)
(*        <2>6 prove B!lt(B!succ(B!zero), morph(a1)) *)
(*             by property morph_succ, morph_zero, morph_lt *)
(*                step <2>5 *)
(*        <2>7 prove B!sd(morph(a1), morph(a2)) *)
(*             by property B!sd_intro *)
(*                step <2>2, <2>4, <2>6 *)
(*        <2>f conclude *)
(*   <1>f conclude; *)

(*   theorem morph_sd_rev : *)
(*      all a1 a2 : Self, *)
(*        B!sd(morph(a1), morph(a2)) -> *)
(*        sd(a1, a2) *)
(*   proof = *)
(*   <1>1 assume a1 a2 : Self, *)
(*        hypothesis H: B!sd(morph(a1), morph(a2)), *)
(*        prove sd(a1, a2) *)
(*        <2>1 prove B!divides(morph(a1), morph(a2)) *)
(*             by property B!sd_divides hypothesis H *)
(*        <2>2 prove divides(a1, a2) *)
(*             by property morph_divides_rev step <2>1 *)
(*        <2>3 prove B!lt(morph(a1), morph(a2)) *)
(*             by property B!sd_lt hypothesis H *)
(*        <2>4 prove lt(a1, a2) *)
(*             by property morph_lt_rev step <2>3 *)
(*        <2>5 prove B!lt(B!succ(B!zero), morph(a1)) *)
(*             by property B!sd_1 hypothesis H *)
(*        <2>6 prove B!lt(morph(succ(zero)), morph(a1)) *)
(*             by property morph_succ, morph_zero step <2>5 *)
(*        <2>7 notation a = succ(zero), *)
(*             prove lt(succ(zero), a1) *)
(*             <3>1 prove B!lt(morph(a), morph(a1)) *)
(*                  by step <2>6 definition of a *)
(*             <3>2 prove lt(a, a1) *)
(*                  by step <3>1 property morph_lt_rev *)
(*             <3>f qed by step <3>2 definition of a *)
(*        <2>8 prove sd(a1, a2) *)
(*             by property sd_intro *)
(*                step <2>2, <2>4, <2>7 *)
(*        <2>f conclude *)
(*   <1>f conclude; *)

(* end;; *)

(* species NatPrimeMorph (B is NatPrime) = *)
(*   inherit NatPrime, NatSDMorph(B); *)

(*   theorem morph_prime : all p : Self, prime(p) -> B!prime(morph(p)) *)
(*   proof = *)
(*   <1>1 assume p : Self, *)
(*        hypothesis H : prime(p), *)
(*        prove B!prime(morph(p)) *)
(*        <2>1 prove lt(succ(zero), p) *)
(*             by property prime_1 hypothesis H *)
(*        <2>2 prove B!lt(B!succ(B!zero), morph(p)) *)
(*             by property morph_lt, morph_succ, morph_zero step <2>1 *)
(*        <2>3 prove (all d : Self, ~(sd(d, p))) *)
(*             by property prime_sd hypothesis H *)
(*        <2>4 prove (all d : B, ~(B!sd(d, morph(p)))) *)
(*             <3>1 assume d : B, *)
(*                  hypothesis Hd : B!sd(d, morph(p)), *)
(*                  prove false *)
(*                  <4>1 prove (ex a : Self, d = morph(a)) *)
(*                       by property morph_surjective *)
(*                  <4>2 assume a : Self, *)
(*                       hypothesis Ha : d = morph(a), *)
(*                       prove false *)
(*                       <5>1 prove B!sd(morph(a), morph(p)) *)
(*                            by hypothesis Hd, Ha *)
(*                       <5>2 prove sd(a, p) *)
(*                            by step <5>1 property morph_sd_rev *)
(*                       <5>3 qed by step <5>2 *)
(*                                   property prime_sd *)
(*                                   hypothesis H *)
(*                  <4>f conclude *)
(*             <3>3 conclude *)
(*        <2>5 qed by step <2>2, <2>4 property B!prime_intro *)
(*    <1>2 conclude; *)

(*   theorem morph_prime_rev : all p : Self, B!prime(morph(p)) -> prime(p) *)
(*   proof = *)
(*   <1>1 assume p : Self, *)
(*        hypothesis H : B!prime(morph(p)), *)
(*        prove prime(p) *)
(*        <2>1 prove B!lt(B!succ(B!zero), morph(p)) *)
(*             by property B!prime_1 hypothesis H *)
(*        <2>2 prove lt(succ(zero), p) *)
(*             by property morph_lt_rev, morph_succ, morph_zero *)
(*             step <2>1 *)
(*        <2>3 prove (all d : B, ~(B!sd(d, morph(p)))) *)
(*             by property B!prime_sd hypothesis H *)
(*        <2>4 prove (all d : Self, ~(sd(d, p))) *)
(*             <3>1 assume d : Self, *)
(*                  hypothesis Hd : sd(d, p), *)
(*                  prove false *)
(*                  <4>1 prove B!sd(morph(d), morph(p)) *)
(*                       by property morph_sd hypothesis Hd *)
(*                  <4>f qed by step <4>1, <2>3 *)
(*             <3>3 conclude *)
(*        <2>5 qed by step <2>2, <2>4 property prime_intro *)
(*    <1>2 conclude; *)
(* end;; *)

(* (\* species NatPrimeDivMorph (B is NatPrimeDiv) = *\) *)
(* (\*   inherit NatPrimeDiv, NatPrimeMorph(B); *\) *)

(* (\*   proof of prime_divisor = *\) *)
(* (\*   <1>1 assume n : Self, *\) *)
(* (\*        hypothesis H : ~(n = succ(zero)), *\) *)
(* (\*        prove ex p : Self, (prime(p) /\ divides(p, n)) *\) *)
(* (\*        <2>1 hypothesis H2 : morph(n) = B!succ(B!zero), *\) *)
(* (\*             prove false *\) *)
(* (\*             <3>1 prove B!succ(B!zero) = morph(succ(zero)) *\) *)
(* (\*                  by property morph_succ, morph_zero *\) *)
(* (\*             <3>2 prove morph(n) = morph(succ(zero)) *\) *)
(* (\*                  by hypothesis H2 step <3>1 *\) *)
(* (\*             <3>3 prove morph(n) = morph(succ(zero)) -> (n = succ(zero)) *\) *)
(* (\*                  by property morph_injective *\) *)
(* (\*             <3>4 prove n = succ(zero) *\) *)
(* (\*                  by step <3>2, <3>3 *\) *)
(* (\*             <3>f qed by step <3>4 hypothesis H *\) *)
(* (\*        <2>2 prove ex p : B, (B!prime(p) /\ B!divides(p, morph(n))) *\) *)
(* (\*             by step <2>1 property B!prime_divisor *\) *)
(* (\*        <2>3 assume p : B, *\) *)
(* (\*             hypothesis H2 : (B!prime(p) /\ B!divides(p, morph(n))), *\) *)
(* (\*             assume p2 : Self, *\) *)
(* (\*             hypothesis H3: p = morph(p2), *\) *)
(* (\*             prove prime(p2) /\ divides(p2, n) *\) *)
(* (\*             <3>1 prove B!prime(morph(p2)) *\) *)
(* (\*                  by hypothesis H2, H3 *\) *)
(* (\*             <3>2 prove prime(p2) *\) *)
(* (\*                  by step <3>1 property morph_prime_rev *\) *)
(* (\*             <3>3 prove B!divides(morph(p2), morph(n)) *\) *)
(* (\*                  by hypothesis H2, H3 *\) *)
(* (\*             <3>4 prove divides(p2, n) *\) *)
(* (\*                  by step <3>3 property morph_divides_rev *\) *)
(* (\*             <3>f conclude *\) *)
(* (\*        <2>f qed by step <2>2, <2>3 property morph_surjective *\) *)
(* (\*   <1>f conclude; *\) *)

(* (\*   let primediv_pred(m, n) = B!primediv_pred(morph(m), morph(n)); *\) *)
(* (\*   proof of primediv_pred_spec = *\) *)
(* (\*   <1>1 assume n p : Self, *\) *)
(* (\*        prove (B!prime(morph(p)) /\ B!divides(morph(p), morph(n))) -> prime(p) *\) *)
(* (\*        by property morph_prime_rev *\) *)
(* (\*   <1>2 assume n p : Self, *\) *)
(* (\*        prove (B!prime(morph(p)) /\ B!divides(morph(p), morph(n))) -> divides(p, n) *\) *)
(* (\*        by property morph_divides_rev *\) *)
(* (\*   <1>3 assume n p : Self, *\) *)
(* (\*        prove (prime(p) /\ divides(p, n)) -> B!prime(morph(p)) *\) *)
(* (\*        by property morph_prime *\) *)
(* (\*   <1>4 assume n p : Self, *\) *)
(* (\*        prove (prime(p) /\ divides(p, n)) -> B!divides(morph(p), morph(n)) *\) *)
(* (\*        by property morph_divides *\) *)
(* (\*   <1>f qed by step <1>1, <1>2, <1>3, <1>4 *\) *)
(* (\*               definition of primediv_pred *\) *)
(* (\*               property B!primediv_pred_spec; *\) *)
(* (\*  end;; *\) *)
