# Copyright Raphaël Cauderlier <raphael.cauderlier@irif.fr>, 2017
#
#    This software is governed by the CeCILL-B license under French law and
# abiding by the rules of distribution of free software.
#
# See "http://www.cecill.info".

-include .config_vars

.config_vars: configure
	./configure

SUBDIRS = lib example
TARGETS = all clean install uninstall

$(TARGETS):
	for d in $(SUBDIRS); do \
		$(MAKE) $@ -C $$d || exit 1; \
	done

.PHONY:	$(TARGETS)
