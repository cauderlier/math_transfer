open "basics";;
open "naturals";;
open "natmorph";; (* For the NatInd species *)

(* An easy way to prove injectivity of successor is to define
   the predecessor function *)
species NatPred =
  inherit NatInd;
  signature pred : Self -> Self;
  property pred_succ : all n : Self, pred(succ(n)) = n;
  (* pred(zero) is unspecified *)

  proof of succ_inj =
  <1>1 assume m n : Self,
       hypothesis H : succ(m) = succ(n),
       prove m = n
       <2>1 prove pred(succ(m)) = pred(succ(n))
            by hypothesis H
       <2>2 prove m = pred(succ(m))
            by property pred_succ
       <2>3 prove pred(succ(n)) = n
            by property pred_succ
       <2>f conclude
  <1>2 conclude;

  theorem case_pred : all n : Self, n = zero \/ n = succ(pred(n))
  proof =
  <1>1 assume n p : Self,
       hypothesis H : n = succ(p),
       prove p = pred(n)
       by hypothesis H property pred_succ
  <1>2 qed by step <1>1 property case;
end;;

(* An easy way to prove 0 != succ (n) is to provide a
   predicate is_zero evaluating to true on 0 and to false on succ *)
species NatIsZero =
  inherit NatInd;

  signature is_zero : Self -> bool;
  property is_zero_zero : is_zero(zero);
  property is_zero_succ : all n : Self, ~ is_zero(succ(n));

  proof of zero_succ = by property is_zero_zero, is_zero_succ;
end;;

(* Iterating a function can be used to define addition and product *)
(* We lack polymorphism here as it is often convenient to
   iterate a function over an other type than natural numbers,
   in particular to define morphisms *)
let ( @ )(f, x) = f(x);;
species NatIter =
  inherit NatInd;
  signature iter : (Self -> Self) -> Self -> Self -> Self;
  property iter_zero : all f : (Self -> Self), all z : Self,
    iter(f, z, zero) = z;
  property iter_succ : all f : (Self -> Self), all z n : Self,
    iter(f, z, succ(n)) = f @ iter(f, z, n);
end;;

species NatIterPlus =
  inherit NatIter, NatPlus;
  let plus(m, n) = iter(succ, n, m);
  proof of zero_plus = by definition of plus property iter_zero;
  proof of succ_plus =
  <1>1 assume k : Self,
       prove succ @ k = succ(k)
       dedukti proof {* (zen.refl abst_T (abst_succ k)) *}
       (* This statement is first-order but the definition of @
          is not so Zenon does not accept it.
          It is however trivial to prove this is Dedukti. *)
  <1>2 qed by step <1>1 definition of plus property iter_succ;
end;;

species NatIterTimes =
  inherit NatIter, NatTimes;
  let times(m, n) = iter(plus(n), zero, m);
  proof of zero_times = by definition of times property iter_zero;
  proof of succ_times =
  <1>1 assume m n : Self,
       prove times(succ(m), n) = plus(n) @ times(m, n)
       by definition of times property iter_succ
  <1>2 assume m n : Self,
       hypothesis H : times(succ(m), n) = plus(n) @ times(m, n),
       prove times(succ(m), n) = plus(n, times(m, n))
       dedukti proof {* H *}
  <1>f conclude;
end;;
