Require Import CoqNaturals.
Section algorithm.

(* Divisibility *)

(* modaux a b = (q, r) <-> q(b+1) + (b - r) = a *)
Fixpoint modaux a b :=
  match a with
    | 0 => (0, b)               (* 0(b+1) + (b - b) = 0 *)
    | S a' =>
      let (q, r) := modaux a' b in (* q(b+1) + (b-r) = a' *)
      match r with
        | 0 => (S q, b)         (* (q+1)(b+1) + (b-b) = q(b+1) + b+1
                                                      = a'+1 = a *)
        | S r' => (q, r')       (* a = a'+1 = q(b+1) + (b-(r'+1)) + 1
                                            = q(b+1) + (b-r') *)
      end
  end.

(* modaux a b = (q, 0) <-> q(b+1) + b = a
                       <-> (q+1)(b+1) = a+1 *)
(* (b+1 | a+1) <-> exists q, modaux a b = (q, 0) *)
(* (b+1 | a+1 strictly) <-> exists q > 0, ... *)

Definition sd b' a' :=
  match a', b' with
    | S a, S (S b) =>
      match modaux a (S b) with
        | (S _, 0) => true
        | _ => false
      end
    | _, _ => false
  end.

Inductive list : Set :=
  | Nil : list
  | Cons : nat -> list -> list.

Fixpoint list_filter (p : nat -> bool) l :=
  match l with
    | Nil => Nil
    | Cons a l =>
      let l' := list_filter p l in
      if p a then Cons a l' else l'
  end.

Fixpoint length l :=
  match l with
    | Nil => 0
    | Cons _ l => S (length l)
  end.

Fixpoint Sieve (l : list) (fuel : nat) {struct fuel} : list :=
  match fuel with
    | 0 => Nil
    | S fuel =>
      match l with
        | Nil => Nil
        | Cons a l => Cons a (Sieve (list_filter (fun b => negb (sd a b)) l) fuel)
      end
  end.

Definition sieve_len l := Sieve l (length l).

Fixpoint interval a b : list :=
  match b with
    | 0 => Cons a Nil
    | S b => Cons a (interval (S a) b)
  end.

Definition eratosthenes n := sieve_len (interval 2 n).
End algorithm.

Inductive le (n :  nat) : nat -> Prop :=
  | le_n : le n n
  | le_S m : le n m -> le n (S m).
Infix "<=" := le.

Definition lt a b := S a <= b.
Infix "<" := lt.

Extraction nat.
Extraction prod.
Extraction modaux.
Extraction bool.
Extraction negb.
Extraction sd.
Extraction list.
Extraction list_filter.
Extraction length.
Extraction Sieve.
Extraction sieve_len.
Extraction interval.
Extraction eratosthenes.
Definition er98 := eratosthenes 98.
Extraction er98.

Section statement.
Inductive Istrue : bool -> Prop := ITT : Istrue true.
Definition prime p := 2 <= p /\ forall d, Istrue (negb (sd d p)).

Fixpoint In n l :=
  match l with
    | Nil => False
    | Cons a l => n = a \/ In n l
  end.
End statement.

Definition is0 (m : nat) : bool :=
  match m with O => true | S _ => false end.

Definition leb := CoqNaturals.leb.

Section addition.

Lemma plus_0 n : n + 0 = n.
Proof.
  induction n.
  - reflexivity.
  - simpl.
    f_equal.
    assumption.
Defined.

Lemma plus_S n m : n + S m = S (n + m).
Proof.
  induction n.
  - reflexivity.
  - simpl.
    f_equal.
    assumption.
Defined.

Lemma plus_commute a b : a + b = b + a.
Proof.
  generalize b; clear b.
  induction a; intro b; simpl.
  - rewrite plus_0.
    reflexivity.
  - rewrite plus_S.
    f_equal.
    intuition.
Defined.

Lemma plus_regular a b c d e : a + b + c = d + b + e ->
                               a + c = d + e.
Proof.
  generalize c; clear c.
  induction a; intro c; simpl.
  - generalize e; clear e; induction d; intro e; simpl.
    + induction b; simpl.
      * congruence.
      * intro H; injection H; intuition.
    + intro H.
      rewrite <- plus_S.
      apply (IHd (S e)).
      rewrite plus_S.
      assumption.
  - intro H.
    rewrite <- plus_S.
    apply (IHa (S c)).
    rewrite plus_S.
    assumption.
Defined.

Lemma plus_regular_right a b c : a + b = c + b -> a = c.
Proof.
  intro H.
  assert (a + 0 = c + 0).
  - apply (plus_regular a b 0 c 0).
    rewrite plus_0.
    rewrite plus_0.
    assumption.
  - rewrite plus_0 in H0.
    rewrite plus_0 in H0.
    assumption.
Defined.
End addition.

Section order.
Lemma le_0 n : 0 <= n.
Proof.
  induction n.
  - apply le_n.
  - apply le_S.
    assumption.
Defined.

Lemma S_increases n1 n2 : n1 <= n2 -> S n1 <= S n2.
Proof.
  intro H.
  induction H.
  - apply le_n.
  - apply le_S.
    assumption.
Defined.

Lemma le_pred_n n : pred n <= n.
Proof.
  destruct n.
  - apply le_n.
  - simpl.
    apply le_S.
    apply le_n.
Defined.

Lemma le_trans a b c : a <= b -> b <= c -> a <= c.
Proof.
  intros H1 H2.
  induction H2.
  - assumption.
  - apply le_S.
    assumption.
Defined.

Lemma le_pred m n : m <= n -> pred m <= pred n.
Proof.
  intro H.
  induction H.
  - apply le_n.
  - simpl.
    apply (le_trans _ _ _ IHle).
    apply le_pred_n.
Defined.

Lemma le_S_n m n : S m <= S n -> m <= n.
Proof.
  intro H.
  apply le_pred in H.
  simpl in H.
  assumption.
Defined.

Lemma lt_irrefl n : ~ (n < n).
Proof.
  intro H.
  induction n.
  - inversion H.
  - apply (le_S_n (S n) n) in H.
    intuition.
Defined.

Lemma le_plus a b : a <= a + b.
Proof.
  induction b.
  - rewrite plus_0.
    apply le_n.
  - rewrite plus_S.
    apply le_S.
    assumption.
Defined.
End order.

Section substraction.
Lemma plus_minus a b c : a = b + c -> a - b = c.
Proof.
  generalize b; clear b.
  induction a; intro b; simpl.
  - induction b; simpl.
    + congruence.
    + discriminate.
  - destruct b; simpl.
    + congruence.
    + intro H; injection H.
      intuition.
Defined.

Lemma plus_then_minus a b c : a + b - (a + c) = b - c.
Proof.
  induction a; simpl.
  - reflexivity.
  - assumption.
Defined.

Lemma n_minus_0 n : n - 0 = n.
Proof.
  induction n; reflexivity.
Defined.

Lemma minus_distrib a b c : (b - c) * a = b * a - c * a.
Proof.
  generalize c; clear c.
  induction b; intro c; simpl.
  - reflexivity.
  - destruct c; simpl.
    + rewrite n_minus_0.
      reflexivity.
    + rewrite (IHb c).
      rewrite plus_then_minus.
      reflexivity.
Defined.
End substraction.

Section product.
Lemma times_commute a b : a * b = b * a.
Proof.
  generalize b; clear b.
  induction a; intro b; simpl.
  - induction b.
    + reflexivity.
    + simpl.
      assumption.
  - induction b.
    + simpl.
      apply IHa.
    + simpl.
      rewrite IHa.
      simpl.
      rewrite <- IHb.
      rewrite IHa.
      f_equal.
      generalize (b * a).
      clear IHa IHb.
      generalize a; clear a.
      induction b; intros a c; simpl.
      * reflexivity.
      * rewrite plus_S.
        f_equal.
        apply IHb.
Defined.

Lemma times_regular a b c : a * (S b) = c * (S b) -> a = c.
Proof.
  generalize c; clear c.
  induction a; intro c; simpl.
  - destruct c; simpl.
    + intuition.
    + discriminate.
  - destruct c; simpl.
    + discriminate.
    + intro H.
      f_equal.
      injection H; clear H.
      intro H.
      apply (plus_regular 0 _ _ 0 _) in H.
      simpl in H.
      intuition.
Defined.
End product.

Section Istrue.

Lemma Istrue_negb b : Istrue b -> ~ Istrue (negb b).
Proof.
  case b; simpl.
  - intros _ H; inversion H.
  - intro H; inversion H.
Defined.

Lemma Istrue_negb_rev b : Istrue (negb b) -> ~ Istrue b.
Proof.
  case b; simpl.
  - intro H; inversion H.
  - intros _ H; inversion H.
Defined.

Lemma Istrue_not_negb b : (~ Istrue b) -> Istrue (negb b).
Proof.
  case b; simpl; intuition.
Defined.

Lemma Istrue_dec b : Istrue b \/ Istrue (negb b).
Proof.
  generalize ITT.
  case b; simpl; intuition.
Defined.
End Istrue.

Section divides_correct.

Lemma modaux_remainder a b q r : (modaux a b = (q, r)) -> r <= b.
Proof.
  generalize q r.
  clear q r.
  induction a; simpl; intros q r H.
  - inversion H.
    apply le_n.
  - destruct (modaux a b) as (q', r').
    generalize (IHa q' r' eq_refl).
    clear IHa.
    generalize H; clear H.
    case r'.
    + intro H; inversion H.
      intros _; apply le_n.
    + intro r''.
      intro H; inversion H.
      clear a q' r' q r'' H H1 H2.
      intro H.
      apply le_S in H.
      apply le_S_n.
      assumption.
Defined.

Lemma modaux_correct a b q r : (modaux a b = (q, r)) -> (r <= b /\ q * (S b) + b = a + r).
Proof.
  generalize b q r.
  clear q r b.
  induction a.
  - simpl.
    intros b q r H.
    inversion H.
    simpl.
    split; [apply le_n | reflexivity].
  - intros b q r.
    intro Hle.
    generalize Hle.
    apply modaux_remainder in Hle.
    generalize (IHa b); clear IHa.
    simpl.
    destruct (modaux a b) as (q', r').
    intro IHa; generalize (IHa q' r' eq_refl).
    clear IHa.
    intros (Hle', H).
    generalize Hle' H.
    clear Hle' H.
    case r'; simpl.
    + rewrite plus_0.
      intros _ H; destruct H; intro He; inversion He.
      split; [apply le_n|].
      simpl.
      generalize (q' * S r).
      intro n.
      clear b q q' r' H0 H1 He Hle.
      apply f_equal.
      assert (r + n = n + r).
      * apply plus_commute.
      * destruct H.
        reflexivity.
    + intro n.
      intros Hle' H He; inversion He.
      destruct H2.
      split; [assumption|].
      generalize H.
      destruct H1.
      generalize (q' * S b).
      intro m.
      intro H'.
      rewrite H'.
      rewrite plus_S.
      reflexivity.
Defined.

Theorem divides_lt a b : Istrue (sd a b) -> a < b.
Proof.
  unfold sd.
  case b ; [ intro H; inversion H | ].
  clear b; intro b.
  case a ; [ intro H; inversion H | ].
  clear a; intro a.
  case a ; [ intro H; inversion H | ].
  clear a; intro a.
  case_eq (modaux b (S a)).
  intros q r.
  intro Hmod.
  apply modaux_correct in Hmod.
  destruct Hmod as (_, He).
  destruct q; [intro H; inversion H|].
  destruct r; [|intro H; inversion H].
  simpl in He.
  rewrite plus_0 in He.
  destruct He.
  intros _.
  apply S_increases.
  apply S_increases.
  apply S_increases.
  generalize (a + q * S (S a)).
  clear q.
  intro n.
  induction n.
  - simpl.
    apply le_S.
    apply le_n.
  - simpl.
    apply le_S.
    assumption.
Defined.

Lemma divides_ge_2 a b : Istrue (sd a b) -> 2 <= a.
Proof.
  unfold sd.
  destruct b.
  - intro H; inversion H.
  - destruct a.
    + intro H; inversion H.
    + destruct a.
      * intro H; inversion H.
      * intros _.
        apply S_increases.
        apply S_increases.
        apply le_0.
Defined.

Lemma divides_correct a b : Istrue (sd a b) -> exists c, c * a = b.
Proof.
  unfold sd.
  destruct b; [intro H; inversion H|].
  destruct a; [intro H; inversion H|].
  destruct a; [intro H; inversion H|].
  case_eq (modaux b (S a)).
  intros q r.
  intro H.
  apply modaux_correct in H.
  destruct H as (_, H).
  destruct q; [intro Hf; inversion Hf|].
  destruct r; [|intro Hf; inversion Hf].
  intros _.
  rewrite plus_0 in H.
  destruct H.
  exists (S (S q)).
  simpl.
  apply f_equal.
  apply f_equal.
  rewrite plus_S.
  apply f_equal.
  rewrite plus_S.
  generalize (q * S (S a)); clear q.
  intro n.
  generalize (a + n).
  clear n.
  intro n.
  rewrite plus_commute.
  rewrite plus_S.
  reflexivity.
Defined.

End divides_correct.

Section divides_complete.

Lemma euclid_uniq_0 a b q r q' : q * (S b) + b = a + S r ->
                                 q' * (S b) + b = a ->
                                 b <= r.
Proof.
  intros H1 H2.
  destruct H2.
  assert (q * S b + b + 0 = q' * S b + b + S r) as H2 by (rewrite plus_0; assumption).
  apply plus_regular in H2.
  rewrite plus_0 in H2.
  apply plus_minus in H2.
  rewrite <- minus_distrib in H2.
  generalize (q - q') H2.
  clear q q' H1 H2.
  destruct n; simpl.
  - discriminate.
  - intro H; injection H; clear H.
    intro H; destruct H.
    generalize (n * S b); clear n.
    induction n.
    + rewrite plus_0; apply le_n.
    + rewrite plus_S; apply le_S; assumption.
Defined.

Lemma euclid_uniq_1 a b q r q' r' : q * (S b) + b = a + r ->
                                    q' * (S b) + b = a + r' ->
                                    r <= b ->
                                    r' <= b ->
                                    r = r'.
Proof.
  generalize r' a; clear r' a.
  induction r.
  - intros r' a H1 H2 H3 H4.
    destruct r'.
    + reflexivity.
    + exfalso.
      rewrite plus_0 in H1.
      assert (b <= r') by apply (euclid_uniq_0 _ _ _ _ _ H2 H1).
      assert (r' < r') by apply (le_trans _ _ _ H4 H).
      apply lt_irrefl in H0.
      assumption.
  - intros r' a H1 H2 H3 H4.
    destruct r'.
    + exfalso.
      rewrite plus_0 in H2.
      assert (b <= r) by apply (euclid_uniq_0 _ _ _ _ _ H1 H2).
      assert (r < r) by apply (le_trans _ _ _ H3 H).
      apply lt_irrefl in H0.
      assumption.
    + f_equal.
      apply (IHr r' (S a)).
      * simpl.
        rewrite plus_S in H1.
        assumption.
      * simpl.
        rewrite plus_S in H2.
        assumption.
      * apply le_S_n.
        apply le_S.
        assumption.
      * apply le_S_n.
        apply le_S.
        assumption.
Defined.

Lemma euclid_uniq_2 a b q r q' r' : q * (S b) + b = a + r ->
                                    q' * (S b) + b = a + r' ->
                                    r <= b ->
                                    r' <= b ->
                                    q = q'.
Proof.
  intros H1 H2 H3 H4.
  assert (r = r') by apply (euclid_uniq_1 _ _ _ _ _ _ H1 H2 H3 H4).
  destruct H.
  destruct H2.
  apply plus_regular_right in H1.
  apply times_regular in H1.
  assumption.
Defined.

Lemma modaux_complete a b q r : r <= b ->
                                q * (S b) + b = a + r ->
                                modaux a b = (q, r).
Proof.
  intros H1 H2.
  case_eq (modaux a b).
  intros q' r'.
  intro H.
  apply modaux_correct in H.
  destruct H as (H3, H4).
  assert (r = r') by apply (euclid_uniq_1 _ _ _ _ _ _ H2 H4 H1 H3).
  assert (q = q') by apply (euclid_uniq_2 _ _ _ _ _ _ H2 H4 H1 H3).
  destruct H.
  destruct H0.
  reflexivity.
Defined.

(* modaux a b = (q, 0) <-> q(b+1) + b = a
                       <-> (q+1)(b+1) = a+1 *)
(* (b+1 | a+1) <-> exists q, modaux a b = (q, 0) *)
(* (b+1 | a+1 strictly) <-> exists q > 0, ... *)

Lemma modaux_0_complete a b q : q * (S b) + b = a ->
                                modaux a b = (q, 0).
Proof.
  intro H.
  apply modaux_complete.
  - apply le_0.
  - rewrite plus_0.
    assumption.
Defined.

Lemma modaux_1_complete a b q : (S q) * (S b) = S a ->
                               modaux a b = (q, 0).
Proof.
  intro H.
  apply modaux_0_complete.
  simpl in H.
  rewrite plus_commute.
  injection H.
  intuition.
Defined.

Lemma divides_complete a b : (exists c, c * a = b) ->
                             2 <= a ->
                             a < b ->
                             Istrue (sd a b).
Proof.
  intros (c, Hc).
  destruct c.
  - simpl in Hc.
    destruct Hc.
    intros _ Ha.
    inversion Ha.
  - destruct b.
    + intros _ Ha.
      inversion Ha.
    + destruct a.
      * intros Hf.
        inversion Hf.
      * apply modaux_1_complete in Hc.
        intros Ha Hab.
        unfold sd.
        apply le_S_n in Ha.
        destruct a; [inversion Ha|].
        rewrite Hc.
        destruct c; [|apply ITT].
        apply modaux_correct in Hc.
        destruct Hc as (_, Hc).
        simpl in Hc.
        rewrite plus_0 in Hc.
        destruct Hc.
        apply lt_irrefl in Hab.
        destruct Hab.
Defined.

End divides_complete.

Section filter.

Lemma filter_length p l : length (list_filter p l) <= length l.
Proof.
  induction l.
  - apply le_n.
  - simpl.
    case (p n).
    + simpl.
      apply S_increases.
      assumption.
    + apply le_S.
      assumption.
Defined.

Lemma filter_correct_1 n p l : In n (list_filter p l) -> In n l.
Proof.
  induction l.
  - intro H; exact H.
  - simpl.
    case (p n0).
    + generalize (list_filter p l) IHl.
      clear p IHl.
      intro l'.
      intros IH H.
      simpl in H; intuition.
    + intro H.
      intuition.
Defined.

Lemma filter_correct_2 n p l : In n (list_filter p l) -> Istrue (p n).
Proof.
  induction l.
  - simpl.
    intro H.
    inversion H.
  - simpl.
    case_eq (p n0).
    + intro He.
      generalize (list_filter p l) IHl.
      clear IHl.
      intros l' IH H.
      destruct H.
      * destruct H.
        rewrite He.
        apply ITT.
      * intuition.
    + intuition.
Defined.

Lemma filter_complete n p l :
  Istrue (p n) ->
  In n l ->
  In n (list_filter p l).
Proof.
  intro Hpn.
  induction l.
  - simpl. intuition.
  - simpl.
    intros [[] | H].
    + generalize (p n) Hpn.
      intros b [].
      simpl.
      intuition.
    + case (p n0); simpl; intuition.
Defined.
End filter.

Section Before.

Inductive Before a b : list -> Prop :=
| Before_In l : In b l -> Before a b (Cons a l)
| Before_Cons c l : Before a b l -> Before a b (Cons c l).

Lemma Before_in_1 a b l : Before a b l -> In a l.
Proof.
  intro H.
  induction H; simpl.
  - intuition.
  - intuition.
Defined.

Lemma Before_in_2 a b l : Before a b l -> In b l.
Proof.
  intro H; induction H; simpl.
  - intuition.
  - intuition.
Defined.

Lemma Before_in_cons a b c l : Before a b (Cons c l) ->
                               a = c \/ Before a b l.
Proof.
  intro H.
  inversion H; intuition.
Defined.

Lemma before_filter a b p l : Before a b (list_filter p l) -> Before a b l.
Proof.
  induction l.
  - simpl; intuition.
  - simpl.
    case (p n).
    + assert (forall x, In x (list_filter p l) -> In x l) by (intro; apply filter_correct_1).
      generalize (list_filter p l) IHl H.
      clear IHl H.
      intros l' IH Hsub Hbefore.
      inversion Hbefore.
      * apply Before_In.
        apply Hsub.
        assumption.
      * apply Before_Cons.
        intuition.
    + intro Hbefore.
      apply Before_Cons.
      intuition.
Defined.

Lemma filter_preserves_order a b l p :
  Before a b l ->
  Istrue (p a) ->
  Istrue (p b) ->
  Before a b (list_filter p l).
Proof.
  intro Hbefore.
  induction Hbefore; simpl.
  - case (p a); simpl.
    + intros _ Hb.
      apply Before_In.
      apply filter_complete; assumption.
    + intro Hf; inversion Hf.
  - intros Ha Hb.
    case (p c).
    + apply Before_Cons.
      apply IHHbefore; assumption.
    + apply IHHbefore; assumption.
Defined.

End Before.

Section sorted.

Definition sorted l := forall a b, Before a b l -> a <= b.

Lemma sorted_cons x l : sorted (Cons x l) -> sorted l.
Proof.
  intros H a b Hbefore.
  apply H.
  apply Before_Cons.
  assumption.
Defined.

End sorted.

Section interval.

Lemma interval_correct_1 a b c : In c (interval a b) -> a <= c.
Proof.
  generalize a; clear a; induction b; intro a; simpl.
  - intuition.
    destruct H0.
    apply le_n.
  - intros [He | Hin].
    + destruct He.
      apply le_n.
    + apply le_S_n.
      apply le_S.
      intuition.
Defined.

Lemma interval_correct_2 a b c : In c (interval a b) -> c <= a + b.
Proof.
  generalize a; clear a; induction b; intro a; simpl.
  - intuition.
    destruct H0.
    rewrite plus_0.
    apply le_n.
  - intros [He | Hin].
    + clear IHb.
      destruct He.
      apply le_plus.
    + rewrite plus_S.
      apply (IHb (S a)).
      assumption.
Defined.

Lemma interval_complete a b c : a <= c -> c <= a + b -> In c (interval a b).
Proof.
  generalize a c; clear a c.
  induction b; intros a c.
  - simpl.
    intro H; inversion H.
    + intuition.
    + intro H2.
      right.
      apply (lt_irrefl m).
      refine (le_trans _ _ _ _ H0).
      rewrite plus_0 in H2.
      assumption.
  - intro Hac.
    inversion Hac.
    + simpl.
      intuition.
    + simpl.
      intro Hmb.
      right.
      apply IHb.
      * apply S_increases.
        assumption.
      * rewrite plus_S in Hmb.
        assumption.
Defined.

Lemma interval_sorted a b : sorted (interval a b).
Proof.
  generalize a; clear a.
  induction b; intro a; simpl.
  - unfold sorted.
    intros x y Hb.
    generalize (Before_in_1 _ _ _ Hb).
    generalize (Before_in_2 _ _ _ Hb).
    intros Hy Hx.
    simpl in Hx, Hy.
    assert (x = y).
    + transitivity a; intuition.
    + destruct H.
      apply le_n.
  - intros x y Hb.
    destruct (Before_in_cons _ _ _ _ Hb).
    + apply Before_in_2 in Hb.
      simpl in Hb.
      destruct Hb.
      * destruct H; destruct H0; apply le_n.
      * apply interval_correct_1 in H0.
        apply le_S_n.
        apply le_S.
        destruct H.
        assumption.
    + apply (IHb (S a) _ _ H).
Defined.

Lemma interval_before a b c d :
  a <= b ->
  b < c ->
  c <= a + d ->
  Before b c (interval a d).
Proof.
  generalize a b c; clear a b c.
  induction d; intros a b c Hab Hbc Hcd.
  - rewrite plus_0 in Hcd.
    assert (c <= b) by apply (le_trans _ _ _ Hcd Hab).
    assert (b < b) by apply (le_trans _ _ _ Hbc H).
    apply lt_irrefl in H0.
    intuition.
  - simpl.
    rewrite plus_S in Hcd.
    destruct Hab.
    + apply Before_In.
      apply interval_complete; assumption.
    + apply Before_Cons.
      apply IHd; try assumption.
      apply S_increases.
      assumption.
Defined.

End interval.

Section Sieve_correct.

Lemma Sieve_correct_1 b l fuel : In b (Sieve l fuel) -> In b l.
Proof.
  generalize l; clear l.
  induction fuel; simpl.
  - intuition.
  - intro l.
    destruct l as [|a l].
    + intuition.
    + simpl.
      intuition.
      apply IHfuel in H0.
      apply filter_correct_1 in H0.
      intuition.
Defined.

Lemma divides_irrefl a : Istrue (negb (sd a a)).
Proof.
  case (Istrue_dec (sd a a)).
  - intro H.
    apply divides_lt in H.
    apply lt_irrefl in H.
    contradiction.
  - intuition.
Defined.

Lemma Sieve_correct_2 fuel a b:
  forall l,
    sorted l ->
    length l <= fuel ->
    Before a b (Sieve l fuel) ->
    Istrue (negb (sd a b)).
Proof.
  generalize a b.
  clear a b.
  induction fuel; intros a b.
  - intros l _ _ H.
    inversion H.
  - intros l Hsorted Hlen Hbefore.
    destruct l; simpl in Hbefore; [inversion Hbefore|].
    destruct (Before_in_cons _ _ _ _ Hbefore) as [He | Hin].
    + destruct He.
      clear IHfuel Hlen.
      destruct (Before_in_2 _ _ _ Hbefore).
      * destruct H.
        apply divides_irrefl.
      * apply Sieve_correct_1 in H.
        apply filter_correct_2 in H.
        assumption.
    + refine (IHfuel a b _ _ _ Hin).
      * generalize (fun b0 : nat => negb (sd n b0)).
        intro p.
        apply sorted_cons in Hsorted.
        clear fuel a b IHfuel n Hlen Hbefore Hin.
        intros a b Hb.
        apply (Hsorted a b).
        eapply before_filter.
        exact Hb.
      * simpl in Hlen.
        apply le_S_n in Hlen.
        refine (le_trans _ _ _ _ Hlen).
        apply filter_length.
Defined.

Lemma sieve_before a b fuel l :
  Before a b l ->
  In a (Sieve l fuel) ->
  In b (Sieve l fuel) ->
  Istrue (sd a b) ->
  sorted l ->
  Before a b (Sieve l fuel).
Proof.
  generalize l; clear l.
  induction fuel as [|fuel]; simpl.
  - intuition.
  - intros l Hbefore; destruct Hbefore.
    + intros _ [Heab|Hbin].
      * destruct Heab.
        intro Hdiv.
        apply divides_lt in Hdiv.
        apply lt_irrefl in Hdiv.
        contradiction.
      * intros _ _.
        apply Before_In.
        assumption.
    + intros [Hea|Ha] [Heb|Hb].
      * destruct Hea.
        destruct Heb.
        intros Hdiv.
        apply divides_lt in Hdiv.
        apply lt_irrefl in Hdiv.
        contradiction.
      * destruct Hea.
        intros _ _.
        apply Before_In.
        assumption.
      * intros Hdiv Hsorted.
        destruct Heb.
        apply Sieve_correct_1 in Ha.
        apply filter_correct_1 in Ha.
        generalize (Hsorted b a (Before_In b a l Ha)).
        intro Hle.
        apply divides_lt in Hdiv.
        destruct (lt_irrefl a (le_trans _ _ _ Hdiv Hle)).
      * intros Hdiv Hsorted.
        apply Before_Cons.
        apply IHfuel; try assumption.
        {
          apply Sieve_correct_1 in Ha.
          apply Sieve_correct_1 in Hb.
          apply filter_correct_2 in Ha.
          apply filter_correct_2 in Hb.
          apply filter_preserves_order; assumption.
        }
        {
          intros a' b' Hbf.
          apply before_filter in Hbf.
          apply (Before_Cons _ _ c) in Hbf.
          apply Hsorted.
          assumption.
        }
Defined.

End Sieve_correct.

Section Sieve_complete.

Lemma Sieve_complete fuel l a :
  length l <= fuel ->
  prime a ->
  In a l ->
  In a (Sieve l fuel).
Proof.
  generalize l; clear l.
  induction fuel; intros [|b l]; simpl.
  - intuition.
  - intro H; inversion H.
  - intuition.
  - intros Hlen Hprime [He | Hin].
    + intuition.
    + right.
      apply IHfuel; try assumption.
      * apply le_S_n in Hlen.
        refine (le_trans _ (length l) _ _ Hlen).
        apply filter_length.
      * apply filter_complete; try assumption.
        destruct Hprime as (_, Hprime).
        intuition.
Defined.

End Sieve_complete.

Lemma eratosthenes_complete p n :
  prime p ->
  p <= S (S n) ->
  In p (eratosthenes n).
Proof.
  intros Hp Hn.
  apply Sieve_complete.
  - apply le_n.
  - assumption.
  - apply interval_complete.
    + unfold prime in Hp.
      intuition.
    + assumption.
Defined.

Section Interop.

(* Imported signature *)
Variable hdivides : nat -> nat -> Prop.
Variable hdiv_times : forall a c, hdivides a (a * c).
Variable hsd : nat -> nat -> Prop.
Variable hquo : nat -> nat -> nat.
Variable heq : nat -> nat -> Prop.
Variable heq_refl : forall m, heq m m.
Variable IT : bool -> Prop.
Variable IT_true : IT true.
Variable IT_false : ~ IT false.
Variable leq_refl1 : forall m n, heq m n -> IT (leb m n).
Variable leq_refl2 : forall m n, heq m n -> IT (leb n m).

Variable hdiv_quo : forall m n, hdivides m n -> heq (m * (hquo m n)) n.
Variable hsd_div : forall m n, hsd m n -> hdivides m n.
Variable hdiv_leb : forall m n, hdivides m n ->
                                  IT (leb 2 n) ->
                                  IT (leb m n).
Variable hsd_lt : forall m n, hsd m n -> IT (leb (S m) n).
Variable hsd_1 : forall m n, hsd m n -> IT (leb (S (S O)) m).
Variable hsd_intro : forall m n, hdivides m n ->
                                   IT (leb (S m) n) ->
                                   IT (leb (S (S O)) m) ->
                                   hsd m n.

Variable hprime : nat -> Prop.
Variable hp_1 : forall p, hprime p -> IT (leb (S (S O)) p).
Variable hp_sd : forall p d, hprime p -> hsd d p -> IT false.
Variable hp_intro : forall p, IT (leb (S (S O)) p) ->
                                (forall d, hsd d p -> IT false) ->
                                hprime p.

(* Imported Theorem *)
Variable hprimediv : nat -> nat.
Variable hpd_prime : forall n, (heq n 1 -> IT false) -> hprime (hprimediv n).
Variable hpd_div : forall n, (heq n 1 -> IT false) -> hdivides (hprimediv n) n.

Section smallest_prime_divisor.

Lemma leb_correct m n : Istrue(leb m n) -> m <= n.
Proof.
  generalize n; clear n.
  induction m; intros [|n].
  - intro; apply le_n.
  - intro; apply le_0.
  - simpl. intro H; inversion H.
  - simpl.
    intro H.
    apply S_increases.
    intuition.
Defined.

Lemma leb_complete m n : m <= n -> Istrue (leb m n).
Proof.
  generalize n; clear n.
  induction m; simpl.
  - intuition.
  - intros [| n].
    + intro H; inversion H.
    + intro H2.
      apply le_S_n in H2.
      intuition.
Defined.

Lemma IT_Istrue b : IT b <-> Istrue b.
Proof.
  case b.
  - intuition.
  - split.
    + intuition.
    + intro H; inversion H.
Defined.

Lemma hsd_complete m n : Istrue (sd m n) -> hsd m n.
Proof.
  intro H.
  apply hsd_intro.
  - apply divides_correct in H.
    destruct H as (c, H).
    rewrite times_commute in H.
    rewrite <- H.
    apply hdiv_times.
  - apply divides_lt in H.
    apply IT_Istrue.
    apply leb_complete.
    assumption.
  - apply divides_ge_2 in H.
    apply IT_Istrue.
    apply leb_complete.
    assumption.
Defined.

Lemma hprime_correct n : hprime n -> prime n.
Proof.
  intro H.
  unfold prime.
  split.
  - apply leb_correct.
    apply IT_Istrue.
    apply hp_1.
    assumption.
  - intro d.
    apply Istrue_not_negb.
    intro Hsd.
    apply (hp_sd _ d) in H.
    + intuition.
    + apply hsd_complete.
      assumption.
Defined.

Lemma leb_lt m n : Istrue (leb m n) ->
                   (Istrue (leb (S m) n) \/ m = n).
Proof.
  generalize m; clear m.
  induction n; simpl; intros [|m] H; intuition.
  simpl in H.
  destruct (IHn m H); intuition.
Defined.

Lemma leb_antisym m n : Istrue (leb m n) ->
                        Istrue (leb n m) ->
                        m = n.
Proof.
  generalize n; clear n; induction m;
  intros [| n]; simpl; intros H1 H2.
  - reflexivity.
  - inversion H2.
  - inversion H1.
  - f_equal.
    apply IHm; assumption.
Defined.

Lemma heq_eq m n : heq m n <-> m = n.
Proof.
  split.
  - intro; apply leb_antisym; apply IT_Istrue; intuition.
  - intro H; destruct H; apply heq_refl.
Defined.

Lemma hsd_correct m n : hsd m n -> Istrue (sd m n).
Proof.
  intro H.
  apply divides_complete.
  - apply hsd_div in H.
    exists (hquo m n).
    rewrite times_commute.
    apply heq_eq.
    apply hdiv_quo.
    assumption.
  - apply hsd_1 in H.
    apply leb_correct.
    apply IT_Istrue.
    assumption.
  - apply hsd_lt in H.
    apply leb_correct.
    apply IT_Istrue.
    assumption.
Defined.

Lemma prime_divisor n : 2 <= n ->
                         (prime n) \/ (Istrue (sd (hprimediv n) n)
                                              /\ prime (hprimediv n)).
Proof.
  intro H2.
  assert (heq n 1 -> IT false).
  - intro H.
    apply heq_eq in H.
    rewrite H in H2.
    inversion H2.
    inversion H1.
  - destruct (Istrue_dec (sd (hprimediv n) n)).
    + right.
      intuition.
      apply hprime_correct.
      intuition.
    + left.
      assert (Istrue (leb (hprimediv n) n)) as H3.
      * apply IT_Istrue.
        apply hdiv_leb.
        intuition.
        apply IT_Istrue.
        apply leb_complete.
        assumption.
      * apply leb_lt in H3.
        destruct H3 as [H3 | H3].
        {
          exfalso.
          apply Istrue_negb_rev in H0.
          apply H0.
          apply hsd_correct.
          apply hsd_intro.
          - intuition.
          - apply IT_Istrue.
            assumption.
          - apply hp_1. intuition.
        }
        {
          rewrite <- H3.
          apply hprime_correct.
          intuition.
        }
Defined.

End smallest_prime_divisor.

Section eratosthenes_correct.

Lemma eratosthenes_correct_1 p n : In p (eratosthenes n) -> p <= S (S n).
Proof.
  intro Hin.
  unfold eratosthenes in Hin.
  unfold sieve_len in Hin.
  apply Sieve_correct_1 in Hin.
  apply interval_correct_2 in Hin.
  assumption.
Defined.

Lemma eratosthenes_correct_2 p n : In p (eratosthenes n) -> 2 <= p.
Proof.
  intro Hin.
  unfold eratosthenes in Hin.
  unfold sieve_len in Hin.
  apply Sieve_correct_1 in Hin.
  apply interval_correct_1 in Hin.
  assumption.
Defined.

Lemma eratosthenes_before a b n :
  Before a b (eratosthenes n) ->
  Istrue (negb (sd a b)).
Proof.
  unfold eratosthenes.
  unfold sieve_len.
  intros H.
  refine (Sieve_correct_2 _ _ _ _ _ _ H).
  - apply interval_sorted.
  - apply le_n.
Defined.

Lemma eratosthenes_correct_3 p n :
  In p (eratosthenes n) -> prime p.
Proof.
  intro H.
  destruct (prime_divisor p) as [ | (Hdiv, Hprime)].
  - apply eratosthenes_correct_2 in H.
    assumption.
  - assumption.
  - exfalso.
    apply (Istrue_negb _ Hdiv).
    apply (eratosthenes_before _ p n).
    assert (Before (hprimediv p) p (interval 2 n)).
    + unfold prime in Hprime.
      apply divides_lt in Hdiv.
      apply interval_before; intuition.
      apply eratosthenes_correct_1 in H.
      assumption.
    + apply sieve_before; try assumption.
      * apply (eratosthenes_complete _ n); try assumption.
        apply eratosthenes_correct_1 in H.
        apply divides_lt in Hdiv.
        apply le_S_n.
        apply le_S.
        apply (le_trans _ p); assumption.
      * apply interval_sorted.
Defined.

End eratosthenes_correct.
End Interop.