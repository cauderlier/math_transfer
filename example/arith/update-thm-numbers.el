

(defun find-thm-number-in-buffer (statement)
  "Find the theorem number corresponding to STATEMENT in the current buffer visiting HolNaturals.txt"
  (goto-char (point-min))
  (forward-line 1)
  (while (not (eolp))
    (search-forward (concat ": " statement)))
  (beginning-of-line)
  (forward-word 3)
  (word-at-point))

(defun find-thm-number (statement)
  "Find the theorem number corresponding to STATEMENT in file HolNaturals.txt"
  (with-temp-buffer
    (insert-file-contents "HolNaturals.txt")
    (find-thm-number-in-buffer statement) ))

(defun update-thm-number-on-previous-line (statement)
  "Update the previous line by replacing any occurence of thm_XXX by thm_YYY where YYY is the number given by find-thm-number"
  (let ((thm-number (find-thm-number statement))
        (end (point)))
    (forward-line (- 1))
    (beginning-of-line)
    (replace-regexp "thm_[0-9]+" (concat "thm_" thm-number) nil (point) end)
    )
  )

(defun update-all-thm-numbers ()
  (interactive)
  (goto-char (point-min))
  (while (search-forward "update-thm-number-on-previous-line")
    (forward-word (- 6))
    (forward-char (- 1))
    (forward-sexp 1)
    (eval-last-sexp nil)
    (forward-line 2))
  )

(provide 'update-thm-numbers)
