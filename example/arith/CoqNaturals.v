Fixpoint leb (n1 n2 : nat) :=
  match n1, n2 with
  | O, _ => true
  | S _, O => false
  | S m1, S m2 => leb m1 m2
  end.
