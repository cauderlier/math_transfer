(* Copyright Ali Assaf <ali.assaf@inria.fr>
             and Raphaël Cauderlier <raphael.cauderlier@inria.fr>, 2015

   This software is governed by the CeCILL-B license under French law and
abiding by the rules of distribution of free software.

See "http://www.cecill.info".  *)


(* HOL types are Coq inhabited sets. *)

Definition Type1 := Type.

Definition Type0 : Type1 := Set.

Inductive type : Type1 :=
  | inhabited : forall (A : Type0), A -> type.

Definition carrier (A : type) : Type0 :=
  match A with inhabited B b => B end.


Definition choice (A : type) : carrier A :=
  match A with inhabited B b => b end.

Definition arrow (A : type) (B : type) : type :=
  inhabited (carrier A -> carrier B)
            (fun _ => choice B).
